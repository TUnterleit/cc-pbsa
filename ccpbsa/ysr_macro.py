# YASARA BioTools
# Visit www.yasara.org for more...
# Copyright by Elmar Krieger

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


import os,string,disk,types
import sys
#  ======================================================================
#                        Y S R _ M A C R O   C L A S S
#  ======================================================================

class interface:
  """
  This class provides an interface to YASARA
  Create a class instance passing the command to run YASARA and the name
  of an error handling function.
  - Instance.error contains an error description if something went wrong
  - Instance.clear() clears the macro
  - Instance.write() adds a line (if argument is a string) or multiple lines
    (if argument is a list) to the macro. Line feeds are added automatically.
  - Instance.submit() executes the macro in YASARA
  
  # EXAMPLE (CONVERSION FROM .sce TO .pdb):
  filename="test"
  macro=ysr_macro("PathToYASARAExecutable")
  macro.write(["LoadSCE %s"%filename,
               "SavePDB all,%s"%filename"])
  macro.submit()
  """

  # OPEN MACRO FILE FOR OUTPUT
  # ==========================
  # - yasara IS THE COMMAND TO RUN YASARA.
  def __init__(self,yasara,errorfunc=None):
    self.yasara=yasara
    self.errorfunc=errorfunc
    self.filename=disk.tmpfilename("startup")+".mcr"
    self.clear()
    #### SET TO 1 IF YOU WANT TO RUN YASARA WITH GRAPHICS
    ###self.graphics=0
    # SET TO "gra" IF YOU WANT TO RUN YASARA WITH GRAPHICS
    self.mode="txt"

  # RAISE AN ERROR
  # ==============
  # CALLS THE ERRORFUNCTION PROVIDED BY THE USER WITH THE GIVEN STRING
  def raiseerror(self,errormsg):
    errormsg=self.__class__.__name__+'.'+errormsg
    self.error=errormsg
    if (self.errorfunc!=None): apply(self.errorfunc,[errormsg])
    else:
      print(errormsg)
      raise SystemExit

  # CLEAR MACRO
  # ===========
  # THE FILE "startup.mcr" IS REOPENED, ALL DATA WRITTEN TO THE MACRO IS LOST.
  def clear(self):
    self.error=None
    self.macrofile=None

  # ADD A LINE TO THE MACRO
  # =======================
  # data IS EITHER A STRING OR A LIST OF STRINGS, LINE FEEDS WILL BE ADDED WHEN MISSING.
  def write(self,data):
    if (self.macrofile==None):
      try: self.macrofile=open(self.filename,"w")
      except: self.raiseerror("write: YASARA macro %s could not be created"%self.filename)
    if (type(data)!=list): data=[data]
    for line in data:
      line=line.rstrip()
      if (line.find("\n")!=-1):
        # A MULTI LINE STRING
        sublinelist=line.split("\n")
        # COUNT MINIMUM NUMBER OF LEADING SPACES
        spacesmin=999999
        for i in range(len(sublinelist)):
          subline=sublinelist[i].rstrip()
          if (subline!=""):
            spacesmin=min(spacesmin,len(subline)-len(subline.lstrip()))
          sublinelist[i]=subline  
        for i in range(len(sublinelist)):
          self.macrofile.write(sublinelist[i][spacesmin:]+"\n")
      else: self.macrofile.write(line+"\n")

  # SUBMIT MACRO
  # ============
  def submit(self):
    self.write("Exit")
    ret=None
    if (self.macrofile==None): self.raiseerror("submit: Tried to submit empty YASARA macro")
    self.macrofile.close()
    if (self.mode=="gra"): flags=""
    else: flags="-txt"
    sys.stdout.flush()
    command=self.yasara+" "+flags+" ."+os.sep+self.filename
    #print "Running YASARA with '%s'"%command
    try: ret=os.system(command)
    except:
      self.raiseerror("submit: YASARA could not be run")
      return(None)
    if (ret): self.raiseerror("submit: YASARA returned error %d (macro %s, workdir %s)"%(ret,self.filename,os.getcwd()))
    #disk.remove(self.filename)
    return(ret)
